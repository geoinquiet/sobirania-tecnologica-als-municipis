# Propostes per a una tecnologia democràtica

## Objectius
  * Posar a l’abast de tothom les innovacions tecnològiques que l'administració incorpora en la seva acció municipal.
  * Pal·liar la distància creada a partir dels diferents usos tecnològics per part dels diferents sectors de la societat (escletxa digital), tant per accés com per coneixement.
  * Vetllar per un accés no discriminador a tràmits i accions digitals de l'administració.
  * Acompanyar tota acció que impliqui una interacció digital amb la ciutadania de la formació necessària per a que tot tècnic municipal pugui guiar a la ciutadania.
  * Promoure una relació conscient i crítica de la ciutadania amb la tecnologia.
===== Objectius programàtics =====
  * Posar en marxa programes de formació i capacitació digital per a la ciutadania, o bé reforçar-ne els existents.
  * Oferir espais de connexió i ús tecnològic que ofereixin eines i recursos de manera gratuïta, i dotar de recursos als existents (biblioteques, centres cívics o telecentres).
  * Oferir programes pedagògics en coordinació amb centres educatius de diferents nivells que permetin abordar com treballar la tecnologia de manera inclusiva i respectuosa amb la ciutadania.
  * Oferir formació tecnològica (inicial i avançada) a col·lectius amb risc d’exclusió social per motius socioeconòmics, en temàtiques d’innovació tecnològica, en coordinació amb els serveis socials municipals.
  * Garantir que el personal de l'administració podrà acompanyar a la ciutadania en la seva interacció digital amb l'administació a partir de programes de formació interna.

## Experiències inspiradores
Pendent
## Justificació dels objectius i proposta programàtica
Pendent




---------
  * Democratització de la tecnologia (fa referència a la part més social, de difusió, educació, ... . S'ha de trobar un titol més apropriat)
      -  Promoure la formació i la capacitació en les noves tecnologies dins i fora de l'administració, per democratitzar l'accés a la tecnologia, eliminar l'escletxa digital i permetre una relació amb la tecnologia més conscient
