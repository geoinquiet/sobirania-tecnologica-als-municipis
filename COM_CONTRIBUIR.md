# Com contribuir

Les contribucións s'han de fer a través de **propostes de modificació** dels documents que conformen l'[Annex](./annex). Cadascun fa referència a un apartat de La Guia.

La proposta de modificació es pot fer:
- Fent un [Merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) per modificar el document objecte de la proposta i el document de bibliografia en el cas que sigui necessari.
- Enviant un correu a info@sobtec.cat indicant quin document es vol modificar i la modificació que es vols fer, i quins documents s'ha consultat per redactar la proposta. Aquests documents apareixeran a l'apartat de bibliografia.

Totes les aportacions seran debatudes conjuntament pels impulsors de La Guia i els redactors de l'aportació.

## Format
Tos els documents segueixen el format [Markdown (GitLab flavored)](https://docs.gitlab.com/ee/user/markdown.html).

## Reconeixement
Tothom qui hagi fet una aportació al document pot demanar aparèixer a l'apartat d'entitats participants com a contribuidor.

Tota la documentació consultada que ha servit de referència per a fer els textos de La Guia haurà d'aparèixer a l'apartat de bibliografia.
